<footer class="footer">
    <div class="footer-inner">
        <h3 class="footer-heading">Subscribe to our Newsletter</h3>
        <div class="footer-inner-row">
            <input type="text" class="footer-input" placeholder="Email"/>
            <button class="footer-submit btn">Go</button>
        </div>
        <div class="footer-text">
            <p>
                © City Harbour 2020
            </p>
            <a href="<?php echo get_privacy_policy_url(); ?>">Privacy</a>
        </div>
        <div class="footer-social">
            <?php if($field = get_field('facebook_link','options')) :?>
            <a href="<?php echo $field;?>"> <i class="fab fa-facebook-f"></i></a>
            <?php endif; ?>
            <?php if($field = get_field('instagram_link','options')) :?>
                <a href="<?php echo $field;?>"> <i class="fab fa-instagram"></i></a>
            <?php endif; ?>
            <?php if($field = get_field('linkedin_link','options')) :?>
                <a href="<?php echo $field;?>"> <i class="fab fa-linkedin-in"></i></a>
            <?php endif; ?>

        </div>
    </div>
    
</footer>
<?php wp_footer(); ?>
</body>
</html>