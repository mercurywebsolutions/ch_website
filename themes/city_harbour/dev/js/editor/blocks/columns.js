registerBlockType('mercury/columns', {
    title: 'Columns',
    description: 'A block for placing left and right content.',
    category: 'layout',
    icon: 'editor-table',
    keywords: ['mercury', 'columns'],
    styles: [
        {
            name: 'default',
            label: 'Default',
            isDefault: true,
        },
    ],
    supports: {
        align: [
            'wide',
            'full',
        ],
        className: false,
    },
    edit: ({ attributes, setAttributes }) => {
        return (
            <div className={'wp-block-columns'}>
                <InnerBlocks
                    allowedBlocks={['mercury/column']}
                    renderAppender={
                        () => <InnerBlocks.ButtonBlockAppender />
                    }
                />
            </div>
        );
    },
    save: ({ attributes }) => {
        return (
            <div className={'wp-block-columns'}>
                <InnerBlocks.Content />
            </div>
        );
    },
});