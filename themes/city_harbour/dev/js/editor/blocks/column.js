registerBlockType('mercury/column', {
    title: 'Column',
    description: 'A block that sits inside the Columns block.',
    category: 'layout',
    icon: 'editor-table',
    keywords: ['mercury', 'column'],
    attributes: {
        columnsSmall: {
            type: 'integer',
            default: 12,
        },
        columnsMedium: {
            type: 'integer',
            default: 12,
        },
        columnsTablet: {
            type: 'integer',
            default: 12,
        },
        columnsLarge: {
            type: 'integer',
            default: 12,
        },
        columnsXLarge: {
            type: 'integer',
            default: 12,
        },
    },
    parent: ['mercury/columns'],
    supports: {
        className: false,
    },
    edit: ({ attributes, setAttributes }) => {
        return (
            <Fragment>
                {
                    <InspectorControls>
                        <PanelBody
                            title={'Columns'}
                            initialOpen
                        >
                            <RangeControl
                                label={'Mobile (portrait)'}
                                value={attributes.columnsSmall}
                                min={1}
                                max={12}
                                onChange={(columnsSmall) => setAttributes({ columnsSmall })}
                            />
                            <RangeControl
                                label={'Mobile (landscape)'}
                                value={attributes.columnsMedium}
                                min={1}
                                max={12}
                                onChange={(columnsMedium) => setAttributes({ columnsMedium })}
                            />
                            <RangeControl
                                label={'Tablet (portrait)'}
                                value={attributes.columnsTablet}
                                min={1}
                                max={12}
                                onChange={(columnsTablet) => setAttributes({ columnsTablet })}
                            />
                            <RangeControl
                                label={'Tablet (landscape)'}
                                value={attributes.columnsLarge}
                                min={1}
                                max={12}
                                onChange={(columnsLarge) => setAttributes({ columnsLarge })}
                            />
                            <RangeControl
                                label={'Desktop'}
                                value={attributes.columnsXLarge}
                                min={1}
                                max={12}
                                onChange={(columnsXLarge) => setAttributes({ columnsXLarge })}
                            />
                        </PanelBody>
                    </InspectorControls>
                }
                <div className={
                    'wp-block-column columns-small-' + attributes.columnsSmall
                    + ' columns-medium-' + attributes.columnsMedium
                    + ' columns-tablet-' + attributes.columnsTablet
                    + ' columns-large-' + attributes.columnsLarge
                    + ' columns-xlarge-' + attributes.columnsXLarge
                }>
                    <InnerBlocks
                        renderAppender={
                            () => <InnerBlocks.ButtonBlockAppender />
                        }
                    />
                </div>
            </Fragment>
        );
    },
    save: ({ attributes }) => {
        return (
            <div className={
                'wp-block-column columns-small-' + attributes.columnsSmall
                + ' columns-medium-' + attributes.columnsMedium
                + ' columns-tablet-' + attributes.columnsTablet
                + ' columns-large-' + attributes.columnsLarge
                + ' columns-xlarge-' + attributes.columnsXLarge
            }>
                <InnerBlocks.Content />
            </div>
        );
    },
});