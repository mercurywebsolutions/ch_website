// a_ in front of file name to ensure it gets loaded before others in folder
// obviously need to do this a better way

const { registerBlockType } = wp.blocks;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { PanelBody, RangeControl } = wp.components;
const { Fragment } = wp.element;