registerBlockType('mercury/swiper-slide', {
    title: 'Swiper Slide',
    description: 'A slide that sits inside the Swiper block.',
    category: 'layout',
    icon: 'slides',
    keywords: ['mercury', 'swiper', 'slide', 'carousel'],
    parent: ['mercury/swiper'],
    supports: {
        className: false,
    },
    edit: ({ attributes, setAttributes }) => {
        return (
            <div className={'wp-block-swiper-slide swiper-slide'}>
                <InnerBlocks
                    renderAppender={
                        () => <InnerBlocks.ButtonBlockAppender />
                    }
                />
            </div>
        );
    },
    save: ({ attributes }) => {
        return (
            <div className={'wp-block=swiper-slide swiper-slide'}>
                <InnerBlocks.Content />
            </div>
        );
    },
});