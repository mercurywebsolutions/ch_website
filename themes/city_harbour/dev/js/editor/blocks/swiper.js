registerBlockType('mercury/swiper', {
    title: 'Swiper',
    description: 'A block for swiping through content in slides.',
    category: 'layout',
    icon: 'slides',
    keywords: ['mercury', 'swiper', 'slide', 'carousel'],
    styles: [
        {
            name: 'default',
            label: 'Default',
            isDefault: true,
        },
    ],
    supports: {
        align: [
            'wide',
            'full',
        ],
        className: false,
    },
    edit: ({ attributes, setAttributes }) => {
        return (
            <div className={'wp-block-swiper swiper-container'}>
                <div class="swiper-wrapper">
                    <InnerBlocks
                        allowedBlocks={['mercury/swiper-slide']}
                        renderAppender={
                            () => <InnerBlocks.ButtonBlockAppender />
                        }
                    />
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-pagination"></div>
            </div>
        );
    },
    save: ({ attributes }) => {
        return (
            <div className={'wp-block-swiper swiper-container'}>
                <div class="swiper-wrapper">
                    <InnerBlocks.Content />
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-pagination"></div>
            </div>
        );
    },
});