registerBlockType('mercury/grid-item', {
    title: 'Grid Item',
    description: 'A block that sits inside the Grid block.',
    category: 'layout',
    icon: 'grid-view',
    keywords: ['mercury', 'grid', 'columns', 'item'],
    attributes: {
        columnsSmall: {
            type: 'integer',
            default: 1,
        },
        columnsMedium: {
            type: 'integer',
            default: 1,
        },
        columnsTablet: {
            type: 'integer',
            default: 1,
        },
        columnsLarge: {
            type: 'integer',
            default: 1,
        },
        columnsXLarge: {
            type: 'integer',
            default: 1,
        },
    },
    parent: ['mercury/grid'],
    supports: {
        className: false,
    },
    edit: ({ attributes, setAttributes }) => {
        return (
            <Fragment>
                {
                    <InspectorControls>
                        <PanelBody
                            title={'Columns'}
                            initialOpen
                        >
                            <RangeControl
                                label={'Mobile (portrait)'}
                                value={attributes.columnsSmall}
                                min={1}
                                max={4}
                                onChange={(columnsSmall) => setAttributes({ columnsSmall })}
                            />
                            <RangeControl
                                label={'Mobile (landscape)'}
                                value={attributes.columnsMedium}
                                min={1}
                                max={4}
                                onChange={(columnsMedium) => setAttributes({ columnsMedium })}
                            />
                            <RangeControl
                                label={'Tablet (portrait)'}
                                value={attributes.columnsTablet}
                                min={1}
                                max={4}
                                onChange={(columnsTablet) => setAttributes({ columnsTablet })}
                            />
                            <RangeControl
                                label={'Tablet (landscape)'}
                                value={attributes.columnsLarge}
                                min={1}
                                max={4}
                                onChange={(columnsLarge) => setAttributes({ columnsLarge })}
                            />
                            <RangeControl
                                label={'Desktop'}
                                value={attributes.columnsXLarge}
                                min={1}
                                max={4}
                                onChange={(columnsXLarge) => setAttributes({ columnsXLarge })}
                            />
                        </PanelBody>
                    </InspectorControls>
                }
                <div className={
                    'wp-block-grid-item columns-small-' + attributes.columnsSmall
                    + ' columns-medium-' + attributes.columnsMedium
                    + ' columns-tablet-' + attributes.columnsTablet
                    + ' columns-large-' + attributes.columnsLarge
                    + ' columns-xlarge-' + attributes.columnsXLarge
                }>
                    <InnerBlocks
                        renderAppender={
                            () => <InnerBlocks.ButtonBlockAppender />
                        }
                    />
                </div>
            </Fragment>
        );
    },
    save: ({ attributes }) => {
        return (
            <div className={
                'wp-block-grid-item columns-small-' + attributes.columnsSmall
                + ' columns-medium-' + attributes.columnsMedium
                + ' columns-tablet-' + attributes.columnsTablet
                + ' columns-large-' + attributes.columnsLarge
                + ' columns-xlarge-' + attributes.columnsXLarge
            }>
                <InnerBlocks.Content />
            </div>
        );
    },
});