'use strict';

wp.domReady(function () {
    // button
    wp.blocks.unregisterBlockStyle('core/button', [
        'fill',
        'outline',
    ]);
    wp.blocks.registerBlockStyle('mercury/grid', [
        {
            name: 'link-tiles',
            label: 'Link tiles'
        },
        {
            name: 'our-people',
            label: 'Our people'
        }
    ]);
    wp.blocks.registerBlockStyle('mercury/swiper', [
        {
            name: 'hero',
            label: 'Hero'
        }
    ]);
    wp.blocks.registerBlockStyle('core/latest-posts', [
        {
            name: '3-post',
            label: '3 Post'
        },
    ]);
});

jQuery(document).ready(function ($) {
    $('#wp-block-library-css').remove();
});