'use strict';

jQuery(document).ready(function ($) {
    //navbar
    $('.hamburger').add('.close').click(function () {
        $('.close').toggleClass('is-active');
        $('body').toggleClass('nav-is-open');
    });

    var $target = $($('.navbar-link').attr('href'));

    $('.navbar-link').scrollTo({
        navbar: $('.navbar'),
        destination: $target,
    });

    // add a 'hero' style to the swiper block in editor.js and then apply it
    // in wordpress

    var defaultSwiper = new Swiper('.swiper-container.is-style-default', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        watchOverflow: true,
    });
    var mySwiper = new Swiper('.swiper-container.is-style-hero', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        watchOverflow: true,
    });
});