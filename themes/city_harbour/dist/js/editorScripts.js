'use strict';

wp.domReady(function () {
  // button
  wp.blocks.unregisterBlockStyle('core/button', ['fill', 'outline']);
  wp.blocks.registerBlockStyle('mercury/grid', [{
    name: 'link-tiles',
    label: 'Link tiles'
  }, {
    name: 'our-people',
    label: 'Our people'
  }]);
  wp.blocks.registerBlockStyle('mercury/swiper', [{
    name: 'hero',
    label: 'Hero'
  }]);
  wp.blocks.registerBlockStyle('core/latest-posts', [{
    name: '3-post',
    label: '3 Post'
  }]);
});
jQuery(document).ready(function ($) {
  $('#wp-block-library-css').remove();
}); // a_ in front of file name to ensure it gets loaded before others in folder
// obviously need to do this a better way

const {
  registerBlockType
} = wp.blocks;
const {
  InspectorControls,
  InnerBlocks
} = wp.blockEditor;
const {
  PanelBody,
  RangeControl
} = wp.components;
const {
  Fragment
} = wp.element;
registerBlockType('mercury/column', {
  title: 'Column',
  description: 'A block that sits inside the Columns block.',
  category: 'layout',
  icon: 'editor-table',
  keywords: ['mercury', 'column'],
  attributes: {
    columnsSmall: {
      type: 'integer',
      default: 12
    },
    columnsMedium: {
      type: 'integer',
      default: 12
    },
    columnsTablet: {
      type: 'integer',
      default: 12
    },
    columnsLarge: {
      type: 'integer',
      default: 12
    },
    columnsXLarge: {
      type: 'integer',
      default: 12
    }
  },
  parent: ['mercury/columns'],
  supports: {
    className: false
  },
  edit: ({
    attributes,
    setAttributes
  }) => {
    return /*#__PURE__*/React.createElement(Fragment, null, /*#__PURE__*/React.createElement(InspectorControls, null, /*#__PURE__*/React.createElement(PanelBody, {
      title: 'Columns',
      initialOpen: true
    }, /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Mobile (portrait)',
      value: attributes.columnsSmall,
      min: 1,
      max: 12,
      onChange: columnsSmall => setAttributes({
        columnsSmall
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Mobile (landscape)',
      value: attributes.columnsMedium,
      min: 1,
      max: 12,
      onChange: columnsMedium => setAttributes({
        columnsMedium
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Tablet (portrait)',
      value: attributes.columnsTablet,
      min: 1,
      max: 12,
      onChange: columnsTablet => setAttributes({
        columnsTablet
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Tablet (landscape)',
      value: attributes.columnsLarge,
      min: 1,
      max: 12,
      onChange: columnsLarge => setAttributes({
        columnsLarge
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Desktop',
      value: attributes.columnsXLarge,
      min: 1,
      max: 12,
      onChange: columnsXLarge => setAttributes({
        columnsXLarge
      })
    }))), /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-column columns-small-' + attributes.columnsSmall + ' columns-medium-' + attributes.columnsMedium + ' columns-tablet-' + attributes.columnsTablet + ' columns-large-' + attributes.columnsLarge + ' columns-xlarge-' + attributes.columnsXLarge
    }, /*#__PURE__*/React.createElement(InnerBlocks, {
      renderAppender: () => /*#__PURE__*/React.createElement(InnerBlocks.ButtonBlockAppender, null)
    })));
  },
  save: ({
    attributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-column columns-small-' + attributes.columnsSmall + ' columns-medium-' + attributes.columnsMedium + ' columns-tablet-' + attributes.columnsTablet + ' columns-large-' + attributes.columnsLarge + ' columns-xlarge-' + attributes.columnsXLarge
    }, /*#__PURE__*/React.createElement(InnerBlocks.Content, null));
  }
});
registerBlockType('mercury/columns', {
  title: 'Columns',
  description: 'A block for placing left and right content.',
  category: 'layout',
  icon: 'editor-table',
  keywords: ['mercury', 'columns'],
  styles: [{
    name: 'default',
    label: 'Default',
    isDefault: true
  }],
  supports: {
    align: ['wide', 'full'],
    className: false
  },
  edit: ({
    attributes,
    setAttributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-columns'
    }, /*#__PURE__*/React.createElement(InnerBlocks, {
      allowedBlocks: ['mercury/column'],
      renderAppender: () => /*#__PURE__*/React.createElement(InnerBlocks.ButtonBlockAppender, null)
    }));
  },
  save: ({
    attributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-columns'
    }, /*#__PURE__*/React.createElement(InnerBlocks.Content, null));
  }
});
registerBlockType('mercury/grid-item', {
  title: 'Grid Item',
  description: 'A block that sits inside the Grid block.',
  category: 'layout',
  icon: 'grid-view',
  keywords: ['mercury', 'grid', 'columns', 'item'],
  attributes: {
    columnsSmall: {
      type: 'integer',
      default: 1
    },
    columnsMedium: {
      type: 'integer',
      default: 1
    },
    columnsTablet: {
      type: 'integer',
      default: 1
    },
    columnsLarge: {
      type: 'integer',
      default: 1
    },
    columnsXLarge: {
      type: 'integer',
      default: 1
    }
  },
  parent: ['mercury/grid'],
  supports: {
    className: false
  },
  edit: ({
    attributes,
    setAttributes
  }) => {
    return /*#__PURE__*/React.createElement(Fragment, null, /*#__PURE__*/React.createElement(InspectorControls, null, /*#__PURE__*/React.createElement(PanelBody, {
      title: 'Columns',
      initialOpen: true
    }, /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Mobile (portrait)',
      value: attributes.columnsSmall,
      min: 1,
      max: 4,
      onChange: columnsSmall => setAttributes({
        columnsSmall
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Mobile (landscape)',
      value: attributes.columnsMedium,
      min: 1,
      max: 4,
      onChange: columnsMedium => setAttributes({
        columnsMedium
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Tablet (portrait)',
      value: attributes.columnsTablet,
      min: 1,
      max: 4,
      onChange: columnsTablet => setAttributes({
        columnsTablet
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Tablet (landscape)',
      value: attributes.columnsLarge,
      min: 1,
      max: 4,
      onChange: columnsLarge => setAttributes({
        columnsLarge
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Desktop',
      value: attributes.columnsXLarge,
      min: 1,
      max: 4,
      onChange: columnsXLarge => setAttributes({
        columnsXLarge
      })
    }))), /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-grid-item columns-small-' + attributes.columnsSmall + ' columns-medium-' + attributes.columnsMedium + ' columns-tablet-' + attributes.columnsTablet + ' columns-large-' + attributes.columnsLarge + ' columns-xlarge-' + attributes.columnsXLarge
    }, /*#__PURE__*/React.createElement(InnerBlocks, {
      renderAppender: () => /*#__PURE__*/React.createElement(InnerBlocks.ButtonBlockAppender, null)
    })));
  },
  save: ({
    attributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-grid-item columns-small-' + attributes.columnsSmall + ' columns-medium-' + attributes.columnsMedium + ' columns-tablet-' + attributes.columnsTablet + ' columns-large-' + attributes.columnsLarge + ' columns-xlarge-' + attributes.columnsXLarge
    }, /*#__PURE__*/React.createElement(InnerBlocks.Content, null));
  }
});
wp.blocks.registerBlockCollection('mercury', {
  title: 'Mercury'
});
registerBlockType('mercury/grid', {
  title: 'Grid',
  description: 'A block that allows you to set the number of items per row on different screen sizes.',
  category: 'layout',
  icon: 'grid-view',
  keywords: ['mercury', 'grid', 'columns'],
  styles: [{
    name: 'default',
    label: 'Default',
    isDefault: true
  }],
  attributes: {
    columnsSmall: {
      type: 'integer',
      default: 1
    },
    columnsMedium: {
      type: 'integer',
      default: 2
    },
    columnsTablet: {
      type: 'integer',
      default: 3
    },
    columnsLarge: {
      type: 'integer',
      default: 3
    },
    columnsXLarge: {
      type: 'integer',
      default: 3
    }
  },
  supports: {
    align: ['wide', 'full'],
    className: false
  },
  edit: ({
    attributes,
    setAttributes
  }) => {
    return /*#__PURE__*/React.createElement(Fragment, null, /*#__PURE__*/React.createElement(InspectorControls, null, /*#__PURE__*/React.createElement(PanelBody, {
      title: 'Columns',
      initialOpen: true
    }, /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Mobile (portrait)',
      value: attributes.columnsSmall,
      min: 1,
      max: 4,
      onChange: columnsSmall => setAttributes({
        columnsSmall
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Mobile (landscape)',
      value: attributes.columnsMedium,
      min: 1,
      max: 4,
      onChange: columnsMedium => setAttributes({
        columnsMedium
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Tablet (portrait)',
      value: attributes.columnsTablet,
      min: 1,
      max: 4,
      onChange: columnsTablet => setAttributes({
        columnsTablet
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Tablet (landscape)',
      value: attributes.columnsLarge,
      min: 1,
      max: 4,
      onChange: columnsLarge => setAttributes({
        columnsLarge
      })
    }), /*#__PURE__*/React.createElement(RangeControl, {
      label: 'Desktop',
      value: attributes.columnsXLarge,
      min: 1,
      max: 4,
      onChange: columnsXLarge => setAttributes({
        columnsXLarge
      })
    }))), /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-grid columns-small-' + attributes.columnsSmall + ' columns-medium-' + attributes.columnsMedium + ' columns-tablet-' + attributes.columnsTablet + ' columns-large-' + attributes.columnsLarge + ' columns-xlarge-' + attributes.columnsXLarge
    }, /*#__PURE__*/React.createElement(InnerBlocks, {
      allowedBlocks: ['mercury/grid-item'],
      renderAppender: () => /*#__PURE__*/React.createElement(InnerBlocks.ButtonBlockAppender, null)
    })));
  },
  save: ({
    attributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-grid columns-small-' + attributes.columnsSmall + ' columns-medium-' + attributes.columnsMedium + ' columns-tablet-' + attributes.columnsTablet + ' columns-large-' + attributes.columnsLarge + ' columns-xlarge-' + attributes.columnsXLarge
    }, /*#__PURE__*/React.createElement(InnerBlocks.Content, null));
  }
});
registerBlockType('mercury/swiper-slide', {
  title: 'Swiper Slide',
  description: 'A slide that sits inside the Swiper block.',
  category: 'layout',
  icon: 'slides',
  keywords: ['mercury', 'swiper', 'slide', 'carousel'],
  parent: ['mercury/swiper'],
  supports: {
    className: false
  },
  edit: ({
    attributes,
    setAttributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-swiper-slide swiper-slide'
    }, /*#__PURE__*/React.createElement(InnerBlocks, {
      renderAppender: () => /*#__PURE__*/React.createElement(InnerBlocks.ButtonBlockAppender, null)
    }));
  },
  save: ({
    attributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block=swiper-slide swiper-slide'
    }, /*#__PURE__*/React.createElement(InnerBlocks.Content, null));
  }
});
registerBlockType('mercury/swiper', {
  title: 'Swiper',
  description: 'A block for swiping through content in slides.',
  category: 'layout',
  icon: 'slides',
  keywords: ['mercury', 'swiper', 'slide', 'carousel'],
  styles: [{
    name: 'default',
    label: 'Default',
    isDefault: true
  }],
  supports: {
    align: ['wide', 'full'],
    className: false
  },
  edit: ({
    attributes,
    setAttributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-swiper swiper-container'
    }, /*#__PURE__*/React.createElement("div", {
      class: "swiper-wrapper"
    }, /*#__PURE__*/React.createElement(InnerBlocks, {
      allowedBlocks: ['mercury/swiper-slide'],
      renderAppender: () => /*#__PURE__*/React.createElement(InnerBlocks.ButtonBlockAppender, null)
    })), /*#__PURE__*/React.createElement("div", {
      class: "swiper-button-prev"
    }), /*#__PURE__*/React.createElement("div", {
      class: "swiper-button-next"
    }), /*#__PURE__*/React.createElement("div", {
      class: "swiper-pagination"
    }));
  },
  save: ({
    attributes
  }) => {
    return /*#__PURE__*/React.createElement("div", {
      className: 'wp-block-swiper swiper-container'
    }, /*#__PURE__*/React.createElement("div", {
      class: "swiper-wrapper"
    }, /*#__PURE__*/React.createElement(InnerBlocks.Content, null)), /*#__PURE__*/React.createElement("div", {
      class: "swiper-button-prev"
    }), /*#__PURE__*/React.createElement("div", {
      class: "swiper-button-next"
    }), /*#__PURE__*/React.createElement("div", {
      class: "swiper-pagination"
    }));
  }
});