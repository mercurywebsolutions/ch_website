const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const localServer = require('./local');

var themeScripts = [
    './dev/js/theme/**/*.js',
    './node_modules/swiper/js/swiper.js',
];
var editorScripts = [
    './dev/js/editor/**/*.js',
];

function style() {
    return gulp.src('./dev/scss/**/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}
function styleEditor() {
    return gulp.src('./dev/scss/**/editor.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}

function jsTheme() {
    return gulp.src(themeScripts)
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
}
function jsEditor() {
    return gulp.src(editorScripts)
        .pipe(concat('editorScripts.js'))
        .pipe(babel({}))
        // .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
}

function watch() {
    browserSync.init({
        proxy: 'http://localhost/' + localServer.localServer()
    });

    gulp.watch('./dev/scss/**/*.scss', gulp.parallel(style, styleEditor));
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch(themeScripts, jsTheme).on('change', browserSync.reload);
    gulp.watch(editorScripts, jsEditor);
}

exports.editor = jsEditor;
exports.default = gulp.series(gulp.parallel(style, styleEditor, jsTheme, jsEditor), watch);