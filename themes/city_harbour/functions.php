<?php

/************************************************************************
 * function.php:
 * 		- Include all relevant styles & scripts
 * Note: Index files must contain get_header() to enque styles/scripts
 ***********************************************************************/

function load_theme_assets() {
    wp_dequeue_style('wp-block-library');

    $styleURI = get_template_directory_uri() . '/dist/css/styles.css';
    $scriptURI = get_template_directory_uri() . '/dist/js/scripts.js';

    $style = get_template_directory() . '/dist/css/styles.css';
    $script = get_template_directory() . '/dist/js/scripts.js';

    wp_enqueue_style('theme', $styleURI, array(), filemtime($style), 'all');

    /* WP's default jquery library seems to be out of date? 
    You cannot manipulate css varlables via jquery without 3.2.2 or heigher */
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', array(), null, true);
    wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array(), null, true);
    wp_enqueue_script('themeJS', $scriptURI, array('jquery', 'jquery-ui-core', 'jquery-ui-widget'), filemtime($script), true);
}
add_action('wp_enqueue_scripts', 'load_theme_assets');

function load_editor_assets() {
    $scriptURIeditor = get_template_directory_uri() . '/dist/js/editorScripts.js';
    $scriptEditor = get_template_directory() . '/dist/js/editorScripts.js';

    wp_enqueue_script('editorJS', $scriptURIeditor, array('wp-blocks', 'wp-block-editor', 'wp-components', 'wp-element'), filemtime($scriptEditor), true);
} 
add_action('enqueue_block_editor_assets', 'load_editor_assets');


// register nav menu locations
function register_custom_nav_menus() {
    register_nav_menus(array(
        'main_menu' => 'Main Menu',
    ));
}
add_action('after_setup_theme', 'register_custom_nav_menus');

// allow align wide/full in Gutenberg
add_theme_support('align-wide');
// customise editor colour palette
add_theme_support('editor-color-palette', array(
    array(
        'name' => __('White', 'themeLangDomain'),
        'slug' => 'white',
        'color' => '#FFF',
    ),
    array(
        'name' => __('Primary', 'themeLangDomain'),
        'slug' => 'primary',
        'color' => '#00567F',
    ),
    array(
        'name' => __('Pink', 'themeLangDomain'),
        'slug' => 'pink',
        'color' => '#E8CBCC',
    ),
    array(
        'name' => __('Blue', 'themeLangDomain'),
        'slug' => 'blue',
        'color' => '#CBD8E1',
    ),
    array(
        'name' => __('Green', 'themeLangDomain'),
        'slug' => 'green',
        'color' => '#D0D4C4',
    ),
));
// customise editor gradient palette
add_theme_support('editor-gradient-presets', array(
    array(
        'name'     => __('White to pink', 'themeLangDomain'),
        'gradient' => 'linear-gradient(180deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 50%, rgba(232, 203, 204, 1) 50%, rgba(232, 203, 204, 1) 100%)',
        'slug'     => 'white-to-pink',
    ),
    array(
        'name'     => __('White to blue', 'themeLangDomain'),
        'gradient' => 'linear-gradient(180deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 50%, rgba(203, 216, 225, 1) 50%, rgba(203, 216, 225, 1) 100%)',
        'slug'     => 'white-to-blue',
    ),
    array(
        'name'     => __('White to green', 'themeLangDomain'),
        'gradient' => 'linear-gradient(180deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 50%, rgba(208, 212, 196, 1) 50%, rgba(208, 212, 196, 1) 100%)',
        'slug'     => 'white-to-green',
    ),
    array(
        'name'     => __('Pink to White', 'themeLangDomain'),
        'gradient' => 'linear-gradient(0deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 50%, rgba(232, 203, 204, 1) 50%, rgba(232, 203, 204, 1) 100%)',
        'slug'     => 'pink-to-white',
    ),
    array(
        'name'     => __('Blue to White', 'themeLangDomain'),
        'gradient' => 'linear-gradient(0deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 50%, rgba(203, 216, 225, 1) 50%, rgba(203, 216, 225, 1) 100%)',
        'slug'     => 'blue-to-white',
    ),
    array(
        'name'     => __('Green to White', 'themeLangDomain'),
        'gradient' => 'linear-gradient(0deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 50%, rgba(208, 212, 196, 1) 50%, rgba(208, 212, 196, 1) 100%)',
        'slug'     => 'green-to-white',
    ),
));
// customise editor font sizes
add_theme_support('editor-font-sizes');
add_theme_support('disable-custom-font-sizes');
// allow custom editor styles
add_theme_support('editor-styles');
add_editor_style('dist/css/editor.css');
// add featured images to posts
add_theme_support('post-thumbnails');

// add ACF options page
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}
 
function set_allowed_block_types($allowed_blocks) {
    return array(
        'core/group',
        'core/cover',
        'core/image',
        'core/buttons',
        'core/heading',
        'core/paragraph',
        'core/spacer',
        'core/video',
        'core/embed',
        'core/html',
        'core/block',
        'core/latest-posts',
        'core/shortcode',
        'mercury/grid',
        'mercury/grid-item',
        'mercury/columns',
        'mercury/column',
        'mercury/swiper',
        'mercury/swiper-slide',
    );
}
add_filter('allowed_block_types', 'set_allowed_block_types');