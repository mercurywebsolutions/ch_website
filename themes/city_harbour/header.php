<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0" />
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="navbar">
        <div class="navbar-inner">
                <a class="navbar-logo" href="<?php echo get_site_url(); ?>">
                    City Harbour
                </a>
                <div class="navbar-inner-right">
                    <a class="navbar-link" href="#contact">Enquire</a>
                    <button type="button" class="navbar-hamburg hamburger">
                        <img  src="<?php bloginfo('template_directory'); ?>/dist/images/hamburger.svg">
                    </button>
                </div>
            <nav class="nav">
                <button type="button" class="navbar-close close">
                    <img src="<?php bloginfo('template_directory'); ?>/dist/images/close.svg">
                </button>
                <?php wp_nav_menu(array('theme_location' => 'main_menu', 'menu_id' => '', 'menu_class' => 'nav-menu', 'container' => false)); ?>
            </nav>
        </div>
    </div>